import { Component, OnInit, ViewChild } from "@angular/core";
import { User } from "./user";
import { LoginService } from "./login.service";
import { Location } from "@angular/common";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  @ViewChild("loginForm") loginForm;
  public loginService: LoginService;
  public location: Location;
  public user: User;

  constructor(loginService: LoginService, location: Location) {
    this.loginService = loginService;
    this.location = location;
  }

  ngOnInit() {
    this.user = new User();
  }

  async onSubmit() {
    const isValid = await this.loginService.attemptLogin(this.user);
    this.loginService.emittLoginStatus(isValid);
  }
}
