import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { PageNotFoundComponent } from "../page-not-found/page-not-found.component";
import { DashboardComponent } from "../dashboard/dashboard.component";
/**
 * Commandes
 */
import { AjouterCommandeComponent } from "../commande/ajouter-commande/ajouter-commande.component";
import { ListeCommandeComponent } from "../commande/liste-commande/liste-commande.component";
import { ListeDuJourCommandeComponent } from "../commande/liste-du-jour-commande/liste-du-jour-commande.component";
/**
 * Clients
 */
import { ClientFormulaireComponent } from "../client/client-formulaire/client-formulaire.component";
import { ListeClientComponent } from "../client/liste-client/liste-client.component";

const appRoutes: Routes = [
  { path: "commande/ajouter-commande", component: AjouterCommandeComponent },
  { path: "commande/liste-commande", component: ListeCommandeComponent },
  {
    path: "commande/liste-du-jour-commande",
    component: ListeDuJourCommandeComponent
  },
  {
    path: "client/liste-client",
    component: ListeClientComponent
  },
  {
    path: "client/client-formulaire",
    component: ClientFormulaireComponent
  },
  { path: "dashboard", component: DashboardComponent },
  { path: "", redirectTo: "/dashboard", pathMatch: "full" },
  { path: "**", component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { useHash: true })],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {}

/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
