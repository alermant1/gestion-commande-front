import { Injectable } from "@angular/core";

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { Subject } from "rxjs/Subject";

import { SessionService } from "../session.service";
import { Token } from "./token";
import { User } from "./user";

const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable({
  providedIn: "root"
})
export class LoginService {
  public sessionService: SessionService;
  public loginEmitter$: Subject<boolean> = new Subject<boolean>();

  private baseUrl: String = `${environment.baseUrlBackEnd}user/token`;

  constructor(private http: HttpClient, sessionService: SessionService) {
    this.sessionService = sessionService;
  }

  isConnected(): boolean {
    return this.sessionService.has("TOKEN");
  }

  getToken(): Token {
    return this.sessionService.get("TOKEN") as Token;
  }

  removeToken(): void {
    this.sessionService.remove("TOKEN");
  }

  setToken(token: Token): void {
    this.sessionService.set("TOKEN", token);
  }

  async attemptLogin(user: User): Promise<boolean> {
    try {
      const resultat: Token = await this.http
        .post<Token>(`${this.baseUrl}`, user, httpOptions)
        .toPromise();
      this.setToken(resultat);
      return true;
    } catch (e) {
      console.log("exception", e);
      return false;
    }
  }

  emittLoginStatus(isConnected: boolean): void {
    this.loginEmitter$.next(isConnected);
  }

  logout() {
    this.removeToken();
    this.loginEmitter$.next(false);
  }
}
