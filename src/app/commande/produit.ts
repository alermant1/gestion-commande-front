import { IProduit } from "./iproduit";

export class Produit implements IProduit {
  public id?: number;
  public quantite: number;
  public format: string;
  public ml: string;
  public mlFinition: string;
  public moulure: string;
  public moulureFinition: string;
  public prix: string;
  public ordre: number;

  constructor(obj: IProduit = {} as IProduit) {
    const {
      id,
      quantite = 1,
      format = '',
      ml = '',
      mlFinition = '',
      moulure = '',
      moulureFinition = '',
      prix = '',
      ordre = 0
    } = obj;

    this.id = id;
    this.quantite = quantite;
    this.format = format;
    this.ml = ml;
    this.mlFinition = mlFinition;
    this.moulure = moulure;
    this.moulureFinition = moulureFinition;
    this.prix = prix;
    this.ordre = ordre;
  }
}
