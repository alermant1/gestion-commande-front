import { Component, OnInit, Input } from "@angular/core";
import { ClientService } from "../client.service";
import { Client } from "../client";
@Component({
  selector: "app-client-formulaire",
  templateUrl: "./client-formulaire.component.html",
  styleUrls: ["./client-formulaire.component.css"],
  providers: [ClientService]
})
export class ClientFormulaireComponent implements OnInit {
  @Input() client: Client;
  public isEditing: boolean = false;
  public clientData: Client;

  constructor(private clientService: ClientService) {}

  async ngOnInit() {
    if (this.client) {
      this.isEditing = true;
      this.clientData = new Client(this.client);
    } else {
      this.clientData = new Client();
    }
  }

  async onSubmit() {
    const response = this.clientData.id
      ? this.clientService.update(this.clientData)
      : this.clientService.add(this.clientData);

    try {
      await response;
      for (const i in this.client) {
        this.client[i] = this.clientData[i];
      }
    } catch (e) {
      console.log("erreur", e);
    }
  }
}
