import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Client } from "../client";

@Component({
  selector: "app-informations-client",
  templateUrl: "./informations-client.component.html",
  styleUrls: ["./informations-client.component.css"]
})
export class InformationsClientComponent implements OnInit {
  public client: Client;
  constructor(
    public dialogRef: MatDialogRef<InformationsClientComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { client: Client }
  ) {}

  ngOnInit(): void {
    this.client = this.data.client;
  }

  fermer(): void {
    this.dialogRef.close();
  }
}
