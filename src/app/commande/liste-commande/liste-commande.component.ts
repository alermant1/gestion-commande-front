import { Component, OnInit, ViewChild, Input, OnChanges } from "@angular/core";
import { Router } from "@angular/router";
import {
  MatPaginator,
  MatSort,
  MatTableDataSource,
  MatDialog
} from "@angular/material";
import { AppDialogConfirmComponent } from "../../app-dialog-confirm/app-dialog-confirm.component";
import { ModalModificationProduitComponent } from "../modal-modification-produit/modal-modification-produit.component";
import { CommandeService } from "../commande.service";
import { Commande } from "../commande";
import { Produit } from "../produit";
import { SessionService } from "../../session.service";
@Component({
  selector: "app-liste-commande",
  templateUrl: "./liste-commande.component.html",
  styleUrls: ["./liste-commande.component.css"],
  providers: [CommandeService]
})
export class ListeCommandeComponent implements OnInit, OnChanges {
  @Input() date: Date;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  public chiffreAffaire: number = 0;
  public dataSource: MatTableDataSource<Commande>;
  public isReady: boolean = false;
  public displayedColumns: string[] = [
    "id",
    "date",
    "description",
    "client",
    "nbArticle",
    "modifier",
    "supprimer"
  ];

  constructor(
    private commandeService: CommandeService,
    public dialog: MatDialog,
    private router: Router,
    private sessionService: SessionService
  ) {}

  async ngOnChanges() {
    await this.initData();
  }
  async ngOnInit() {
    await this.initData();
  }

  /**
   * On initialise le tableau, appelé quand on change la valeur du @Input
   */
  async initData() {
    this.isReady = false;
    // si on a une date définit, on affiche uniquement les date
    const data: Commande[] = this.date
      ? await this.commandeService.getCommandesDate(this.date)
      : await this.commandeService.getCommandes();

    this.dataSource = new MatTableDataSource(data);
    const isNumeric = n => !isNaN(parseFloat(n)) && isFinite(n);
    this.chiffreAffaire = 0;
    if (this.date) {
      for (const commande of data) {
        for (const produit of commande.produits) {
          this.chiffreAffaire +=
            (isNumeric(produit.prix) ? parseFloat(produit.prix) : 0) *
            produit.quantite;
        }
      }
    }
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.dataSource.filterPredicate = (commande: Commande, filter: string) => {
      const dataStr: string = (commande.id
          + commande.client.nom
          + commande.description
          + commande.produits.map((p: Produit) => p.quantite
              + p.prix
              + p.format
              + p.moulureFinition
              + p.moulure
              + p.ml
              + p.mlFinition).join('')
      ).toLowerCase();
      return dataStr.indexOf(filter) !== -1;
    };
    this.isReady = true;
  }

  applyFilter(filterValue: string): void {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  supprimer(commande: Commande): void {
    const dialog = this.dialog.open(AppDialogConfirmComponent, {
      width: "250px",
      data: {
        title: "Confirmer la suppression",
        body: "Êtes vous sûr de suprimer?"
      }
    });

    dialog.afterClosed().subscribe(async result => {
      if (result !== true) {
        return;
      }
      try {
        console.log(await this.commandeService.delete(commande));
        this.dataSource.data = this.dataSource.data.filter(c => {
          return c.id !== commande.id;
        });
      } catch (e) {
        console.log("Erreur à la suppression d'une commande", e);
      }
    });
  }

  modifier(commande: Commande): void {
    this.sessionService.set("commandeEnCours", commande);
    this.router.navigate(["/commande/ajouter-commande"]);
  }

  supprimerProduit(produit: Produit, indexCommande: number): void {
    const dialog = this.dialog.open(AppDialogConfirmComponent, {
      width: "250px",
      data: {
        title: "Confirmer la suppression",
        body: "Êtes vous sûr de suprimer?"
      }
    });

    dialog.afterClosed().subscribe(async result => {
      if (result !== true) {
        return;
      }
      try {
        console.log(await this.commandeService.supprimerProduit(produit));
        this.dataSource.data[indexCommande].produits = this.dataSource.data[
          indexCommande
        ].produits.filter(p => {
          return p.id !== produit.id;
        });
      } catch (e) {
        console.log("Erreur à la suppression du produit d'une commande", e);
      }
    });
  }

  modifierProduit(
    produit: Produit,
    indexCommande: number,
    indexProduit: number
  ): void {
    const dialog = this.dialog.open(ModalModificationProduitComponent, {
      width: "500px",
      data: { produit }
    });

    dialog.afterClosed().subscribe(async (newProduit: Produit) => {
      if (!newProduit) {
        return;
      }
      try {
        await this.commandeService.modifierProduit(newProduit);
        // on met à jour le produit
        this.dataSource.data[indexCommande].produits[indexProduit] = newProduit;
      } catch (e) {
        console.log("erreur à l'enregistrement", e);
      }
    });
  }
}
