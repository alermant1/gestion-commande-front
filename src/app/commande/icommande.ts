import { Client } from "../client/client";
import { Produit } from "./produit";

export interface ICommande {
  id?: number;
  description: string;
  date: Date;
  client: Client;
  produits?: Produit[];
}
