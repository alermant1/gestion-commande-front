import { IClient } from "./iclient";

export class Client implements IClient {
  public id?: number;
  public nom: string;
  public raisonSocial: string;
  public adresse: string;
  public codePostal: string;
  public ville: string;

  constructor(obj: IClient = {} as IClient) {
    const {
      id,
      nom = "",
      raisonSocial = "",
      adresse = "",
      codePostal = "",
      ville = ""
    } = obj;

    this.id = id;
    this.nom = nom;
    this.raisonSocial = raisonSocial;
    this.adresse = adresse;
    this.codePostal = codePostal;
    this.ville = ville;
  }
}
