export class AppLien {
  constructor(label: string, url: string) {
    this.label = label;
    this.url = url;
  }

  label: string;
  url: string;
}
