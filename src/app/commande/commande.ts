import { ICommande } from "./icommande";
import { Client } from "../client/client";
import { Produit } from "./produit";

export class Commande implements ICommande {
  public id?: number;
  public description: string;
  public date: Date;
  public client: Client;
  public produits?: Produit[];

  constructor(obj: ICommande = {} as ICommande) {
    const {
      id,
      description = "",
      date = new Date(),
      client = new Client(),
      produits = []
    } = obj;
    this.id = id;
    this.description = description;
    this.date = date;
    this.client = client;
    this.produits = produits;
  }
}
