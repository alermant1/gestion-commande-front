export interface IProduit {
  id?: number;
  quantite: number;
  format: string;
  ml: string;
  mlFinition: string;
  moulure: string;
  moulureFinition: string;
  prix: string;
  ordre: number;
}
