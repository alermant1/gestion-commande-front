import { Component, OnInit, Input, ViewChild } from "@angular/core";
import { MatDialog } from "@angular/material";
import { Commande } from "../commande";
import { Produit } from "../produit";
import { Client } from "../../client/client";
import { CommandeService } from "../commande.service";
import { ClientService } from "../../client/client.service";
import { SessionService } from "../../session.service";
import { AppDialogConfirmComponent } from "../../app-dialog-confirm/app-dialog-confirm.component";

@Component({
  selector: "app-ajouter-commande",
  templateUrl: "./ajouter-commande.component.html",
  styleUrls: ["./ajouter-commande.component.css"],
  providers: [CommandeService, ClientService]
})
export class AjouterCommandeComponent implements OnInit {
  @Input() commande: Commande;
  @ViewChild("commandeForm") commandeForm;
  public SESSION_COMMANDE_EN_COURS: string = "commandeEnCours";
  public produitCallback: Function;
  public currentDate: Date = new Date();
  public isReady: boolean = false;
  public clients: Client[];
  public savedCommandeSession: Commande;

  constructor(
    private commandeService: CommandeService,
    private clientService: ClientService,
    private sessionService: SessionService,
    public dialog: MatDialog
  ) {}

  async ngOnInit() {
    this.produitCallback = this.enregistrerSession.bind(this);
    this.savedCommandeSession = this.sessionService.get(
      this.SESSION_COMMANDE_EN_COURS
    ) as Commande;

    if (!this.commande) {
      this.commande = this.savedCommandeSession
        ? new Commande(this.savedCommandeSession)
        : new Commande();
    }
    this.clients = await this.clientService.getClients();
    this.isReady = true;
  }

  enregistrerSession() {
    this.sessionService.set(this.SESSION_COMMANDE_EN_COURS, this.commande);
  }

  ngAfterViewInit() {
    this.commandeForm.form.valueChanges.subscribe(change => {
      this.enregistrerSession();
    });
  }

  /**
   * On enregistre la commande en base de données et met à jour
   * this.commande (ajout de l'id & du client)
   */
  async onSubmit(withReset: boolean = false) {
    const commandeTemp = new Commande(this.commande);
    commandeTemp.produits = undefined;
    // On met à jour la commande (sans les produits)
    if (commandeTemp.id) {
      await this.commandeService.update(commandeTemp);
    } else {
      // sinon on ajoute la commande
      const { id } = await this.commandeService.add(commandeTemp);
      // et on rajoute l'id
      this.commande.id = id;
    }

    // on supprime tous les produits de la commande
    await this.commandeService.supprimerProduitsCommande(this.commande);

    // on ajoute les produits
    const promisesProduits: Promise<Produit>[] = this.commande.produits.map(
      (produit: Produit, ordre: number) => {
        produit.ordre = ordre;
        return this.commandeService.ajouterProduit(produit, this.commande);
      }
    );

    try {
      // on met à jour les produits enregistré depuis la base de données
      await Promise.all(promisesProduits);
      // si on a enregistré, on supprime en session
      this.sessionService.remove(this.SESSION_COMMANDE_EN_COURS);
      if (withReset) {
        // on reset le formulaire
        this.commande = new Commande();
      }
    } catch (e) {
      console.log("erreur", e);
    }
  }

  /**
   * @description Ajoute un nouveau produit dans la liste this.commande.produits
   */
  ajouterProduit() {
    this.commande.produits.push(new Produit());
  }

  /**
   * @description Supprime le produit (seulement l'affichage)
   * @param index tableau this.commande.produits
   */
  annulerProduit(index: number) {
    this.commande.produits = this.commande.produits.filter((e, i) => {
      return index != i;
    });
  }

  /**
   * @description Duplique l'élement et le rajoute dans le tableau
   * @param index tableau this.commande.produits
   */
  dupliquerProduit(index: number) {
    const produit: Produit = new Produit(this.commande.produits[index]);
    // on supprime l'index
    produit.id = undefined;
    this.commande.produits.push(produit);
  }

  chargerProfilSession() {
    this.commande = new Commande(this.savedCommandeSession);
  }

  resetFormulaire() {
    const dialog = this.dialog.open(AppDialogConfirmComponent, {
      width: "250px",
      data: {
        title: "Confirmer la réinitialisation",
        body:
          "Le formulaire sera réinitialisé."
      }
    });

    dialog.afterClosed().subscribe(async result => {
      if (result !== true) {
        return;
      }
      this.commande = new Commande();
    });
  }

  async enregistrerEtReset() {
    // on enregistre
    await this.onSubmit(true);
  }
}
