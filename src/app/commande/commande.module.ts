import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AjouterCommandeComponent } from "./ajouter-commande/ajouter-commande.component";
import { ListeCommandeComponent } from "./liste-commande/liste-commande.component";
import { ListeDuJourCommandeComponent } from "./liste-du-jour-commande/liste-du-jour-commande.component";
import { MaterialModule } from "../material/material.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgSelectModule, NG_SELECT_DEFAULT_CONFIG } from "@ng-select/ng-select";
import { AjouterProduitComponent } from './ajouter-produit/ajouter-produit.component';
import { ModalModificationProduitComponent } from './modal-modification-produit/modal-modification-produit.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule
  ],
  declarations: [
    AjouterCommandeComponent,
    ListeCommandeComponent,
    ListeDuJourCommandeComponent,
    AjouterProduitComponent,
    ModalModificationProduitComponent
  ],
  providers: [
    {
      provide: NG_SELECT_DEFAULT_CONFIG,
      useValue: {
        notFoundText: "Pas de résultat."
      }
    }
  ]
})
export class CommandeModule {}
