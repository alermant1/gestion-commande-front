import { Injectable, Injector } from "@angular/core";
import {
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpEvent,
  HttpErrorResponse,
  HttpResponse,
  HttpHeaders
} from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";
import { AppToastService } from "./app-toast/app-toast.service";
import { Toast } from "./app-toast/toast.enum";
import { LoginService } from "./login/login.service";

@Injectable()
export class AppInterceptor implements HttpInterceptor {
  appToastService: AppToastService;
  loginService: LoginService;
  constructor(private injector: Injector) {
    this.appToastService = this.injector.get(AppToastService);
    this.loginService = this.injector.get(LoginService);
  }
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // HttpHeader object immutable - copy values
    const headerSettings: { [name: string]: string | string[] } = {};

    for (const key of req.headers.keys()) {
      headerSettings[key] = req.headers.getAll(key);
    }

    if (this.loginService.isConnected()) {
      headerSettings["X-AUTH-TOKEN"] = this.loginService.getToken().token;
    }

    headerSettings["Content-Type"] = "application/json";
    const newHeader = new HttpHeaders(headerSettings);

    const changedRequest = req.clone({
      headers: newHeader
    });

    return next
      .handle(changedRequest)
      .map((evt: HttpEvent<any>) => {
        if (req.method === "OPTIONS") {
          return evt;
        }
        localStorage.setItem("loading", "Y");

        if (evt && evt.type === 0) {
          localStorage.setItem("loading", "N");
        }

        if (!(evt instanceof HttpResponse)) {
          return evt;
        }

        localStorage.setItem("loading", "N");

        if ("POST" === req.method) {
          this.appToastService.addToast(Toast.AJOUT_SUCCESS);
        }

        if ("PUT" === req.method) {
          this.appToastService.addToast(Toast.MODIFICATION_SUCCESS);
        }

        if ("DELETE" === req.method) {
          this.appToastService.addToast(Toast.SUPPRESSION_SUCCESS);
        }

        return evt;
      })
      .catch((err: any, caught) => {
        localStorage.setItem("loading", "N");

        if (err instanceof HttpErrorResponse) {
          this.appToastService.addToast(Toast.ERREUR);
          return Observable.throw(err);
        }
      });
  }
}
