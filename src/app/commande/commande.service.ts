import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { ICommande } from "./icommande";
import { IProduit } from "./iproduit";
import * as moment from "moment";

const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable({
  providedIn: "root"
})
export class CommandeService {
  private baseUrl: String = `${environment.baseUrlBackEnd}api/commande`;
  constructor(private http: HttpClient) {}

  getCommande(id: string): Promise<ICommande> {
    return this.http
      .get<ICommande>(`${this.baseUrl}/${id}`, httpOptions)
      .toPromise();
  }

  getCommandes(): Promise<ICommande[]> {
    return this.http
      .get<ICommande[]>(`${this.baseUrl}/`, httpOptions)
      .toPromise();
  }

  getCommandesDate(date: Date): Promise<ICommande[]> {
    return this.http
      .get<ICommande[]>(
        `${this.baseUrl}/date/${moment(date).format("YYYY-MM-DD")}`
      )
      .toPromise();
  }

  add(commande: ICommande): Promise<ICommande> {
    return this.http
      .post<ICommande>(`${this.baseUrl}/`, commande, httpOptions)
      .toPromise();
  }

  update(commande: ICommande): Promise<ICommande> {
    return this.http
      .put<ICommande>(`${this.baseUrl}/${commande.id}`, commande, httpOptions)
      .toPromise();
  }

  delete(commande: ICommande) {
    return this.http.delete(`${this.baseUrl}/${commande.id}`).toPromise();
  }

  ajouterProduit(produit: IProduit, commande: ICommande): Promise<IProduit> {
    return this.http
      .post<IProduit>(
        `${this.baseUrl}/${commande.id}/produit`,
        produit,
        httpOptions
      )
      .toPromise();
  }

  modifierProduit(produit: IProduit): Promise<IProduit> {
    return this.http
      .put<IProduit>(
        `${this.baseUrl}/produit/${produit.id}`,
        produit,
        httpOptions
      )
      .toPromise();
  }

  supprimerProduit(produit: IProduit) {
    return this.http
      .delete<IProduit>(`${this.baseUrl}/produit/${produit.id}`, httpOptions)
      .toPromise();
  }

  /**
   * Supprime tous les produits d'une commande.
   *
   * @param commande
   */
  supprimerProduitsCommande(commande: ICommande) {
    return this.http
      .delete<IProduit>(`${this.baseUrl}/${commande.id}/produit`, httpOptions)
      .toPromise();
  }

  telechargerRecapitulatif(date: Date): Promise<Blob> {
    const httpOptions = {
      headers: new HttpHeaders({ "Content-Type": "application/json" })
    };

    return this.http
      .get<Blob>(
        `${this.baseUrl}/telecharger/${moment(date).format("YYYY-MM-DD")}`,
        { responseType: "blob" as "json" }
      )
      .toPromise();
  }
}
