import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Produit } from "../produit";

@Component({
  selector: "app-modification-produit",
  templateUrl: "./modal-modification-produit.component.html",
  styleUrls: ["./modal-modification-produit.component.css"]
})
export class ModalModificationProduitComponent implements OnInit {
  public produit: Produit;
  constructor(
    public dialogRef: MatDialogRef<ModalModificationProduitComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { produit: Produit }
  ) {}

  ngOnInit(): void {
    this.produit = new Produit(this.data.produit);
  }

  fermer(): void {
    this.dialogRef.close();
  }

  enregistrerProduit(): void {
    this.dialogRef.close(this.produit);
  }
}
