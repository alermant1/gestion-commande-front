import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class SessionService {
  constructor() {}

  has(key: string) {
    return !!localStorage.getItem(key);
  }

  get(key: string): any {
    return JSON.parse(localStorage.getItem(key));
  }

  set(key: string, value: object | number | string): void {
    const typeOf = typeof value;

    if (typeOf === "object") {
      localStorage.setItem(key, JSON.stringify(value));
      return;
    }

    if (typeOf === "string" || typeOf === "number") {
      localStorage.setItem(key, value as string);
      return;
    }

    throw new Error(`SessionService, typeof value not implemented : ${typeOf}`);
  }

  remove(key: string) {
    localStorage.removeItem(key);
  }
}
