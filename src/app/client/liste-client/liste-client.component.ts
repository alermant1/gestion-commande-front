import { Component, OnInit, ViewChild } from "@angular/core";
import { ClientService } from "../client.service";
import { Client } from "../client";
import {
  MatPaginator,
  MatSort,
  MatTableDataSource,
  MatDialog
} from "@angular/material";
import { AppDialogConfirmComponent } from "../../app-dialog-confirm/app-dialog-confirm.component";
import { InformationsClientComponent } from "../informations-client/informations-client.component";

@Component({
  selector: "app-liste-client",
  templateUrl: "./liste-client.component.html",
  styleUrls: ["./liste-client.component.css"],
  providers: [ClientService]
})
export class ListeClientComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public isReady: boolean = false;
  public displayedColumns: string[] = [
    "id",
    "nom",
    "raisonSocial",
    "adresse",
    "ville",
    "codePostal",
    "modifier",
    "supprimer"
  ];
  public dataSource: MatTableDataSource<Client>;
  constructor(private clientService: ClientService, public dialog: MatDialog) {}

  async ngOnInit() {
    this.dataSource = new MatTableDataSource(
      await this.clientService.getClients()
    );
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.isReady = true;
  }

  ngAfterViewInit() {}

  applyFilter(filterValue: string): void {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  supprimer(client: Client): void {
    const dialog = this.dialog.open(AppDialogConfirmComponent, {
      width: "250px",
      data: {
        title: "Confirmer la suppression",
        body: "Êtes vous sûr de suprimer?"
      }
    });

    dialog.afterClosed().subscribe(async result => {
      if (result !== true) {
        return;
      }
      try {
        console.log(await this.clientService.delete(client));
        this.dataSource.data = this.dataSource.data.filter(c => {
            return c.id !== client.id;
          }
        );
      } catch (e) {
        console.log("exception", e);
      }
    });
  }

  modifier(client: Client): void {
    const dialog = this.dialog.open(InformationsClientComponent, {
      width: "800px",
      data: {
        client: client
      }
    });
  }
}
