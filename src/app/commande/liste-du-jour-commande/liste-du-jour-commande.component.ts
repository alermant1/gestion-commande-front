import { Component, OnInit, ViewChild } from "@angular/core";
import { saveAs } from "file-saver";
import { CommandeService } from "../commande.service";
import * as moment from "moment";

@Component({
  selector: "app-liste-du-jour-commande",
  templateUrl: "./liste-du-jour-commande.component.html",
  styleUrls: ["./liste-du-jour-commande.component.css"]
})
export class ListeDuJourCommandeComponent implements OnInit {
  public date: Date;
  public btnTelechargerDisponible: boolean = true;
  constructor(private commandeService: CommandeService) {}

  ngOnInit() {
    this.date = new Date();
  }

  updateDate() {
    this.date = new Date(this.date);
  }

  async downloadCommande() {
    this.btnTelechargerDisponible = false;
    const blob = await this.commandeService.telechargerRecapitulatif(this.date);
    saveAs(blob, `Commandes-${moment(this.date).format("DD-MM-YYYY")}.pdf`);
    this.btnTelechargerDisponible = true;
  }
}
