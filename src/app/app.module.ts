import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule, LOCALE_ID } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { ToastrModule } from "ngx-toastr";
import { registerLocaleData } from "@angular/common";
import localeFr from "@angular/common/locales/fr";
import localeFrExtra from "@angular/common/locales/extra/fr";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { MaterialModule } from "./material/material.module";
import { AppComponent } from "./app.component";
import { ClientModule } from "./client/client.module";
import { CommandeModule } from "./commande/commande.module";
import { AppRoutingModule } from "./app-routing/app-routing.module";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { AppToastComponent } from "./app-toast/app-toast.component";
import { AppInterceptor } from "./app-interceptor";
import { AppDialogConfirmComponent } from "./app-dialog-confirm/app-dialog-confirm.component";
import { InformationsClientComponent } from "./client/informations-client/informations-client.component";
import { ModalModificationProduitComponent } from "./commande/modal-modification-produit/modal-modification-produit.component";
import { LoginComponent } from "./login/login.component";
import "moment/locale/fr";
import {
  MAT_DATE_LOCALE,
  MAT_DATE_FORMATS,
  DateAdapter
} from "@angular/material/core";
import { MAT_MOMENT_DATE_FORMATS } from "@angular/material-moment-adapter";
import { MomentUtcDateAdapter } from "./moment-utc-adapter.service";

registerLocaleData(localeFr, "fr-FR", localeFrExtra);

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    DashboardComponent,
    AppToastComponent,
    AppDialogConfirmComponent,
    LoginComponent
  ],
  exports: [MaterialModule],
  imports: [
    BrowserModule,
    MaterialModule,
    ClientModule,
    CommandeModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    AppInterceptor,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AppInterceptor,
      multi: true
    },
    { provide: LOCALE_ID, useValue: "fr-fr" },
    { provide: MAT_DATE_LOCALE, useValue: "fr-FR" },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
    { provide: DateAdapter, useClass: MomentUtcDateAdapter }
  ],
  entryComponents: [
    AppDialogConfirmComponent,
    InformationsClientComponent,
    ModalModificationProduitComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
