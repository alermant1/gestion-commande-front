import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

@Component({
  selector: "app-dialog-confirm",
  templateUrl: "./app-dialog-confirm.component.html"
})
export class AppDialogConfirmComponent {
  constructor(
    public dialogRef: MatDialogRef<AppDialogConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { title: string; body: string }
  ) {}

  onYesClick() {
    this.dialogRef.close(true);
  }
  onNoClick() {
    this.dialogRef.close(false);
  }
}
