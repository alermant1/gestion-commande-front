import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { IClient } from "./iclient";

const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable()
export class ClientService {
  private baseUrl: String = `${environment.baseUrlBackEnd}api/client`;
  constructor(private http: HttpClient) {}

  getClient(id: string): Promise<IClient> {
    return this.http
      .get<IClient>(`${this.baseUrl}/${id}`, httpOptions)
      .toPromise();
  }

  getClients(): Promise<IClient[]> {
    return this.http
      .get<IClient[]>(`${this.baseUrl}/`, httpOptions)
      .toPromise();
  }

  add(client: IClient): Promise<IClient> {
    return this.http
      .post<IClient>(`${this.baseUrl}/`, client, httpOptions)
      .toPromise();
  }

  update(client: IClient): Promise<IClient> {
    return this.http
      .put<IClient>(`${this.baseUrl}/${client.id}`, client, httpOptions)
      .toPromise();
  }

  delete(client: IClient) {
    return this.http.delete(`${this.baseUrl}/${client.id}`).toPromise();
  }
}
