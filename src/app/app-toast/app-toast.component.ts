import { Component, DoCheck, OnDestroy } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { AppToastService } from "./app-toast.service";
import { Toast } from "./toast.enum";
import { Subscription } from "rxjs/Subscription";

@Component({
  selector: "app-toast",
  templateUrl: "./app-toast.component.html",
  styleUrls: ["./app-toast.component.css"]
})
export class AppToastComponent implements OnDestroy, DoCheck {
  public loading: Boolean;
  private subscription: Subscription;
  constructor(
    private toastr: ToastrService,
    private toastService: AppToastService
  ) {
    const component = this;
    this.subscription = this.toastService.toastEmitter$.subscribe({
      next(toast: Toast) {
        if (toast === Toast.ERREUR) {
          component.toastr.error("", toast);
        } else {
          component.toastr.success("", toast);
        }
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  ngDoCheck() {
    this.loading = localStorage.getItem("loading") === "Y";
  }
}
