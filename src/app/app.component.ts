import {
  ChangeDetectorRef,
  Component,
  OnInit,
  OnDestroy,
  Input
} from "@angular/core";
import "hammerjs";
import { MediaMatcher } from "@angular/cdk/layout";
import { AppLien } from "./interface/app-lien";
import { LoginService } from "./login/login.service";
import { Subscription } from "rxjs/Subscription";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit, OnDestroy {
  private _mobileQueryListener: () => void;
  mobileQuery: MediaQueryList;
  public liens: Array<AppLien>;
  @Input() isConnected: boolean = false;
  private subscription: Subscription;
  public loginService: LoginService;
  public ref: ChangeDetectorRef;

  ngOnInit() {
    this.liens = [
      new AppLien("Ajouter une commande", "/commande/ajouter-commande"),
      new AppLien("Commandes du jour", "/commande/liste-du-jour-commande"),
      new AppLien("Liste des commandes", "/commande/liste-commande"),
      new AppLien("Liste des clients", "/client/liste-client"),
      new AppLien("Ajouter un client", "/client/client-formulaire")
    ];

    const $ctrl = this;
    this.subscription = this.loginService.loginEmitter$.subscribe({
      next(isValid: boolean) {
        $ctrl.isConnected = isValid;
      }
    });
  }

  logout() {
    this.loginService.logout();
  }

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    loginService: LoginService
  ) {
    this.loginService = loginService;
    this.mobileQuery = media.matchMedia("(max-width: 600px)");
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    this.isConnected = this.loginService.isConnected();
    this.ref = changeDetectorRef;
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
    this.subscription.unsubscribe();
  }
}
