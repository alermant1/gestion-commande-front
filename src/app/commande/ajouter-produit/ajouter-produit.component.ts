import { Component, OnInit, Input, ViewChild } from "@angular/core";
import { Produit } from "../produit";

@Component({
  selector: "app-ajouter-produit",
  templateUrl: "./ajouter-produit.component.html",
  styleUrls: ["./ajouter-produit.component.css"]
})
export class AjouterProduitComponent implements OnInit {
  @Input() produit: Produit;
  @Input() updateForm: Function;
  @ViewChild("produitForm") produitForm;

  constructor() {}

  ngOnInit() {}

  ngAfterViewInit() {
    this.produitForm.form.valueChanges.subscribe(change => {
      if (this.updateForm) {
        this.updateForm();
      }
    });
  }
}
