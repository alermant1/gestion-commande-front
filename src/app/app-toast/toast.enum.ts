export enum Toast {
  AJOUT_SUCCESS = "Ajout effectuée.",
  MODIFICATION_SUCCESS = "Modification effectuée.",
  SUPPRESSION_SUCCESS = "Supression effectuée.",
  ERREUR = "Une erreur est survenue."
}
