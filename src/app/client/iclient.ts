export interface IClient {
  id?: number;
  nom: string;
  raisonSocial: string;
  adresse: string;
  codePostal: string;
  ville: string;
}
