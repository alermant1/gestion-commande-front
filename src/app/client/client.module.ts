import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MaterialModule } from "../material/material.module";
import { ListeClientComponent } from "./liste-client/liste-client.component";
import { ClientFormulaireComponent } from "./client-formulaire/client-formulaire.component";
import { InformationsClientComponent } from "./informations-client/informations-client.component";

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, MaterialModule],
  declarations: [
    ListeClientComponent,
    ClientFormulaireComponent,
    InformationsClientComponent
  ]
})
export class ClientModule {}
