import { Injectable, EventEmitter } from "@angular/core";
import { Toast } from "./toast.enum";
import { Subject } from "rxjs/Subject";

@Injectable({
  providedIn: "root"
})
export class AppToastService {
  public toastEmitter$: Subject<Toast> = new Subject<Toast>();
  constructor() {}
  addToast(toast: Toast) {
    this.toastEmitter$.next(toast);
  }
}
